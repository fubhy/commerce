<?php

$databases = array (
  'default' =>
  array (
    'default' =>
    array (
      'database' => 'commerce',
      'username' => 'root',
      'password' => '',
      'host' => 'commerce.local',
      'port' => '',
      'driver' => 'mysql',
      'prefix' => '',
    ),
  ),
);

$update_free_access = FALSE;

ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);
ini_set('session.gc_maxlifetime', 200000);
ini_set('session.cookie_lifetime', 2000000);
# $cookie_domain = 'example.com';

# $conf['site_name'] = 'My Drupal site';
# $conf['theme_default'] = 'garland';
# $conf['anonymous'] = 'Visitor';

# $conf['maintenance_theme'] = 'bartik';
$conf['reverse_proxy'] = TRUE;
# $conf['reverse_proxy_addresses'] = array('a.b.c.d', ...);
# $conf['omit_vary_cookie'] = TRUE;

# $conf['locale_custom_strings_en'][''] = array(
#   'forum'      => 'Discussion board',
#   '@count min' => '@count minutes',
# );

// Always override the file system paths.
$conf['file_private_path'] = 'sites/default/private';
$conf['file_public_path'] = 'files';
$conf['file_temporary_path'] = '/tmp';

// Always disable CSS and JS aggregation and the anonymous page cache.
$conf['cache'] = FALSE;
$conf['preprocess_css'] = FALSE;
$conf['preprocess_js'] = FALSE;

// Environment indicator.
$conf['environment_indicator_enabled'] = TRUE;
$conf['environment_indicator_color'] = "#0066ff";
$conf['environment_indicator_text'] = 'VAGRANT';

// Specifiy dump location for ddump and dimport.
$conf['drunomics-dump-dir'] = 'sites/default/dev/';

$conf['install_profile'] = 'standard';
