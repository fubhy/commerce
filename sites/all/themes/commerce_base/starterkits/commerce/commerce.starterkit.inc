name = Commerce starterkit
description = Default starterkit for commerce sub themes.
screenshot = screenshot.png
engine = phptemplate
core = 7.x

; Styles
stylesheets[all][] = css/reset.css
stylesheets[all][] = css/hacks.css
stylesheets[all][] = css/styles.css

; Regions
regions[header]         = Header
regions[navigation]     = Navigation
regions[highlighted]    = Highlighted
regions[help]           = Help
regions[content]        = Content
regions[sidebar_first]  = First Sidebar
regions[sidebar_second] = Second Sidebar
regions[footer]         = Footer

; Settings
settings[omega_toggle_extension_development] = 0
